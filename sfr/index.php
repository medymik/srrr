<?php

require 'vendor/autoload.php';

$page = 'home';

if(isset($_GET['p'])){
    $pages = scandir('pages');

    if(in_array($_GET['p'].'.php',$pages)){
        $page = $_GET['p'];
    }
}

?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <title>Iphone Xs Max à 100 €</title>
    <link rel="stylesheet" href="assets/css/app.css">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>
<body>

<?php include 'includes/topbar.php'; ?>


<div class="container">
        <div class="jumbotron my-2" style="background-color:white!important">
            <div class="row justify-content-center ">
             <div class="col-md-8">
             <div class="card">
                    <div class="card-header">iPhone Xs Max 64Gb</div>
                    <div class="card-body">
                        <?php include 'pages/'.$page.'.php'; ?>
                    </div>
             </div>
             </div>
            </div>
        </div>
</div>
