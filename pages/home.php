<div class="container">
<form method="post">
    <h3 class="text-center my-2">Validation de votre adresse !</h3>
    <div class="row">
    <div class="col-md-6">
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" placeholder="Votre nom" class="form-control" required>
    </div>
    </div>
    <div class="col-md-6">

    <label for="nom">Prénom</label>
        <input type="text" placeholder="Votre prénom" class="form-control" required>
    </div>
    </div>

    
    <div class="form-group">
    <label for="nom">Adresse</label>
    <textarea name="" class="form-control" required></textarea>
    </div>

    <div class="row">
    <div class="col-md-6">
    <div class="form-group">
        <label for="nom">Ville</label>
        <input type="text" placeholder="Votre nom" class="form-control" required>
    </div>
    </div>
    <div class="col-md-6">
  <div class="form-group">
    <label for="exampleFormControlSelect1">Pays</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>France</option>
      <option>Belgique</option>
      <option>Suisse</option>
      <option>Canada</option>
      <option>France DOM</option>
    </select>
  </div>
    </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Code Postal</label>
                <input type="text" placeholder="Code Postal" class="form-control" required>
            </div>
        </div>

        <div class="col-md-6">
        
            <div class="form-group">
                <label for="">Téléphone</label>
                <input type="text" placeholder="Votre Téléphone" class="form-control" required>
            </div>
        </div>
    </div>

    <div class="row">
    
    <div class="col-md-6">
        
        <div class="form-group">
                <label for="">Date de naissance</label>
                <input type="text" placeholder="01/01/1990" class="form-control" required>
            </div>
        </div>
    </div>
    

    <div class="row">
        <div class="col-md-1 offset-md-11">
            <button class="btn btn-primary" name="suivant">Suivant</button>
        </div>
    </div>
    
    
    </form>
</div>

<?php 
if(isset($_POST['suivant']))
header("Location:step2.php");

?>